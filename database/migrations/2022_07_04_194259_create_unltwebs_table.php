<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnltwebsTable extends Migration
{
    protected $connection = "mysql";    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unltwebs', function (Blueprint $table) {
            $table->id();
            $table->string('apellido')->nullable();
            $table->string('nombre')->nullable();
            $table->string('correo')->nullable();
            $table->string('entorno')->nullable();
            $table->string('orientacion')->nullable();
            $table->string('conocimientos')->nullable();
            $table->string('tp1_html')->nullable();
            $table->string('tp2_css')->nullable();
            $table->string('tp3_servidores')->nullable();
            $table->string('tp4_http')->nullable();
            $table->string('encuentro')->nullable();
            $table->timestamps();
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unltwebs');
    }
}
