# Proyecto para consultas

###Proyecto en Laravel para consultas
Este proyecto sirve para realizar consultas o para armarte un proyecto de cero. Ya tiene instalado una barra superior, una aplicación principal con Bootstrap, AnimateCss, Datatables (sólo tenes que cargar el id) 

1)
composer require ibex/crud-generator --dev

2)
php artisan vendor:publish --tag=crud

3)
php artisan make:crud nombre_tabla

4 )Agregar Ruta en web.php
Route::resource('leeregistro107', App\Http\Controllers\Controller::class);

Route::resource('banks', 'BankController');

ejemplo
php artisan make:crud registro__local


