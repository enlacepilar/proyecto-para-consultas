<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UnltwebController;

Route::get('', function () {
    return view('inicio');
});

Route::get('/loguear', function () {
    return view('auth.login');
});

Auth::routes();

Route::resource('unltwebs', UnltwebController::class)->middleware('auth'); 


#region HUEVO de PASCUA
    Route::get('/hola', function () {
        return "Hola Caranlito: Huevo de pascua
        <br>
        <h3>GOLEM - Jorge Luis bordes </h1>
    <br>Si (como afirma el griego en el Cratilo)
    <br>el nombre es arquetipo de la cosa
    <br>en las letras de 'rosa' está la rosa
    <br>y todo el Nilo en la palabra 'Nilo'.
    <br>
    <br>Y, hecho de consonantes y vocales,
    <br>habrá un terrible Nombre, que la esencia
    <br>cifre de Dios y que la Omnipotencia
    <br>guarde en letras y sílabas cabales.
    <br>
    <br>Adán y las estrellas lo supieron
    <br>en el Jardín. La herrumbre del pecado
    <br>(dicen los cabalistas) lo ha borrado
    <br>y las generaciones lo perdieron.
    <br>
    <br>Los artificios y el candor del hombre
    <br>no tienen fin. Sabemos que hubo un día
    <br>en que el pueblo de Dios buscaba el Nombre
    <br>en las vigilias de la judería.
    <br>
    <br>No a la manera de otras que una vaga
    <br>sombra insinúan en la vaga historia,
    <br>aún está verde y viva la memoria
    <br>de Judá León, que era rabino en Praga.
    <br>
    <br>Sediento de saber lo que Dios sabe,
    <br>Judá León se dio a permutaciones
    <br>de letras y a complejas variaciones
    <br>y al fin pronunció el Nombre que es la Clave,
    <br>
    <br>la Puerta, el Eco, el Huésped y el Palacio,
    <br>sobre un muñeco que con torpes manos
    <br>labró, para enseñarle los arcanos
    <br>de las Letras, del Tiempo y del Espacio.
    <br>
    <br>El simulacro alzó los soñolientos
    <br>párpados y vio formas y colores
    <br>que no entendió, perdidos en rumores
    <br>y ensayó temerosos movimientos.
    <br>
    <br>Gradualmente se vio (como nosotros)
    <br>aprisionado en esta red sonora
    <br>de Antes, Después, Ayer, Mientras, Ahora,
    <br>Derecha, Izquierda, Yo, Tú, Aquellos, Otros.
    <br>
    <br>(El cabalista que ofició de numen
    <br>a la vasta criatura apodó Golem;
    <br>estas verdades las refiere Scholem
    <br>en un docto lugar de su volumen.)
    <br>
    <br>El rabí le explicaba el universo
    <br>'esto es mi pie; esto el tuyo, esto la soga.'
    <br>y logró, al cabo de años, que el perverso
    <br>barriera bien o mal la sinagoga.
    <br>
    <br>Tal vez hubo un error en la grafía
    <br>o en la articulación del Sacro Nombre;
    <br>a pesar de tan alta hechicería,
    <br>no aprendió a hablar el aprendiz de hombre.
    <br>
    <br>Sus ojos, menos de hombre que de perro
    <br>y harto menos de perro que de cosa,
    <br>seguían al rabí por la dudosa
    <br>penumbra de las piezas del encierro.
    <br>
    <br>Algo anormal y tosco hubo en el Golem,
    <br>ya que a su paso el gato del rabino
    <br>se escondía. (Ese gato no está en Scholem
    <br>pero, a través del tiempo, lo adivino.)
    <br>
    <br>Elevando a su Dios manos filiales,
    <br>las devociones de su Dios copiaba
    <br>o, estúpido y sonriente, se ahuecaba
    <br>en cóncavas zalemas orientales.
    <br>
    <br>El rabí lo miraba con ternura
    <br>y con algún horror. '¿Cómo' (se dijo)
    <br>'pude engendrar este penoso hijo
    <br>y la inacción dejé, que es la cordura?'
    <br>
    <br>'¿Por qué di en agregar a la infinita
    <br>serie un símbolo más? ¿Por qué a la vana
    <br>madeja que en lo eterno se devana,
    <br>di otra causa, otro efecto y otra cuita?'
    <br>
    <br>En la hora de angustia y de luz vaga,
    <br>en su Golem los ojos detenía.
    <br>¿Quién nos dirá las cosas que sentía
    <br>Dios, al mirar a su rabino en Praga?
    <br>
    ";
    });
#endregion