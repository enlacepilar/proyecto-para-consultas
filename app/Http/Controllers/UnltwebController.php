<?php

namespace App\Http\Controllers;

use App\Models\Unltweb;
use Illuminate\Http\Request;

/**
 * Class UnltwebController
 * @package App\Http\Controllers
 */
class UnltwebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $unltwebs = Unltweb::paginate();

        return view('unltweb.index', compact('unltwebs'))
            ->with('i', (request()->input('page', 1) - 1) * $unltwebs->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $unltweb = new Unltweb();
        return view('unltweb.create', compact('unltweb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Unltweb::$rules);

        $unltweb = Unltweb::create($request->all());

        return redirect()->route('unltwebs.index')
            ->with('success', '¡Alumno creado!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $unltweb = Unltweb::find($id);

        return view('unltweb.show', compact('unltweb'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $unltweb = Unltweb::find($id);

        return view('unltweb.edit', compact('unltweb'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Unltweb $unltweb
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Unltweb $unltweb)
    {
        request()->validate(Unltweb::$rules);

        $unltweb->update($request->all());

        return redirect()->route('unltwebs.index')
            ->with('success', '¡Datos del alumno actualizados!');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $unltweb = Unltweb::find($id)->delete();

        return redirect()->route('unltwebs.index')
            ->with('success', 'Alumno borrado de la base.');
    }
}
