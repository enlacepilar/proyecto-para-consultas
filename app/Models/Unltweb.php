<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Unltweb
 *
 * @property $id
 * @property $apellido
 * @property $nombre
 * @property $correo
 * @property $entorno
 * @property $orientacion
 * @property $conocimientos
 * @property $tp1_html
 * @property $tp2_css
 * @property $tp3_servidores
 * @property $tp4_http
 * @property $encuentro
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Unltweb extends Model
{
  protected $table ='unltwebs';
    
    static $rules = [
		'apellido' => 'required',
		'nombre' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['apellido','nombre','correo','entorno','orientacion','conocimientos','tp1_html','tp2_css','tp3_servidores','tp4_http','encuentro'];



}
