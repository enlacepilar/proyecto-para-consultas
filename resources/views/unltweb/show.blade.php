@extends('layouts.admin_raiz')

@section('contenido_app')

@include('layouts.barraSuperiorAdmin')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Muestra info del Alummno</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('unltwebs.index') }}"> Volver</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Apellido:</strong>
                            {{ $unltweb->apellido }}
                        </div>
                        <div class="form-group">
                            <strong>Nombre:</strong>
                            {{ $unltweb->nombre }}
                        </div>
                        <div class="form-group">
                            <strong>Correo:</strong>
                            {{ $unltweb->correo }}
                        </div>
                        <div class="form-group">
                            <strong>Entorno:</strong>
                            {{ $unltweb->entorno }}
                        </div>
                        <div class="form-group">
                            <strong>Orientacion:</strong>
                            {{ $unltweb->orientacion }}
                        </div>
                        <div class="form-group">
                            <strong>Conocimientos:</strong>
                            {{ $unltweb->conocimientos }}
                        </div>
                        <div class="form-group">
                            <strong>Tp1 Html:</strong>
                            {{ $unltweb->tp1_html }}
                        </div>
                        <div class="form-group">
                            <strong>Tp2 Css:</strong>
                            {{ $unltweb->tp2_css }}
                        </div>
                        <div class="form-group">
                            <strong>Tp3 Servidores:</strong>
                            {{ $unltweb->tp3_servidores }}
                        </div>
                        <div class="form-group">
                            <strong>Tp4 Http:</strong>
                            {{ $unltweb->tp4_http }}
                        </div>
                        <div class="form-group">
                            <strong>Encuentro:</strong>
                            {{ $unltweb->encuentro }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
