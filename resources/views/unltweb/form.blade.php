<div class="box box-info padding-1 alert-success p-5 m-3 container">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('apellido') }}
            {{ Form::text('apellido', $unltweb->apellido, ['class' => 'form-control' . ($errors->has('apellido') ? ' is-invalid' : ''), 'placeholder' => 'Apellido']) }}
            {!! $errors->first('apellido', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('nombre') }}
            {{ Form::text('nombre', $unltweb->nombre, ['class' => 'form-control' . ($errors->has('nombre') ? ' is-invalid' : ''), 'placeholder' => 'Nombre']) }}
            {!! $errors->first('nombre', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('correo') }}
            {{ Form::text('correo', $unltweb->correo, ['class' => 'form-control' . ($errors->has('correo') ? ' is-invalid' : ''), 'placeholder' => 'Correo']) }}
            {!! $errors->first('correo', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('entorno') }}
            {{ Form::text('entorno', $unltweb->entorno, ['class' => 'form-control' . ($errors->has('entorno') ? ' is-invalid' : ''), 'placeholder' => 'Entorno']) }}
            {!! $errors->first('entorno', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('orientacion') }}
            {{ Form::text('orientacion', $unltweb->orientacion, ['class' => 'form-control' . ($errors->has('orientacion') ? ' is-invalid' : ''), 'placeholder' => 'Orientacion']) }}
            {!! $errors->first('orientacion', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('conocimientos') }}
            {{ Form::text('conocimientos', $unltweb->conocimientos, ['class' => 'form-control' . ($errors->has('conocimientos') ? ' is-invalid' : ''), 'placeholder' => 'Conocimientos']) }}
            {!! $errors->first('conocimientos', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('tp1_html') }}
            {{ Form::text('tp1_html', $unltweb->tp1_html, ['class' => 'form-control' . ($errors->has('tp1_html') ? ' is-invalid' : ''), 'placeholder' => 'Tp1 Html']) }}
            {!! $errors->first('tp1_html', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('tp2_css') }}
            {{ Form::text('tp2_css', $unltweb->tp2_css, ['class' => 'form-control' . ($errors->has('tp2_css') ? ' is-invalid' : ''), 'placeholder' => 'Tp2 Css']) }}
            {!! $errors->first('tp2_css', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('tp3_servidores') }}
            {{ Form::text('tp3_servidores', $unltweb->tp3_servidores, ['class' => 'form-control' . ($errors->has('tp3_servidores') ? ' is-invalid' : ''), 'placeholder' => 'Tp3 Servidores']) }}
            {!! $errors->first('tp3_servidores', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('tp4_http') }}
            {{ Form::text('tp4_http', $unltweb->tp4_http, ['class' => 'form-control' . ($errors->has('tp4_http') ? ' is-invalid' : ''), 'placeholder' => 'Tp3 Http']) }}
            {!! $errors->first('tp4_http', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('encuentro') }}
            {{ Form::text('encuentro', $unltweb->encuentro, ['class' => 'form-control' . ($errors->has('encuentro') ? ' is-invalid' : ''), 'placeholder' => 'Encuentro']) }}
            {!! $errors->first('encuentro', '<div class="invalid-feedback">:message</div>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary btn-block">Envía / Actualiza</button>
    </div>
</div>