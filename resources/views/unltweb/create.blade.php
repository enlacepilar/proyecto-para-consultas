@extends('layouts.admin_raiz')

@section('contenido_app')

@include('layouts.barraSuperiorAdmin')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title">Alta Alummno</span>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('unltwebs.store') }}"  role="form" enctype="multipart/form-data">
                            @csrf

                            @include('unltweb.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
