@extends('layouts.admin_raiz')

@section('contenido_app')

@include('layouts.barraSuperiorAdmin')

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                Tecnologías Web
                            </span>

                             <div class="float-right">
                                <a href="{{ route('unltwebs.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  Nuevo Alumno
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered" id='tabla_resultados'>
                                <thead class="thead">
                                    <tr>

										<th>Apellido</th>
										<th>Nombre</th>
										{{-- <th>Correo</th> --}}
                                        <th>Tp1 Html</th>
										<th>Tp2 Css</th>
										<th>Tp3 Servidores</th>
										<th>Tp4 Http</th>
                                        <th>Encuentro</th> 
                                        <th>Acciones</th>
										{{-- <th>Entorno</th>
										<th>Orientacion</th>
										<th>Conocimientos</th>--}}
										
										

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($unltwebs as $unltweb)
                                        <tr class="text-center">
                                            {{-- <td>{{ ++$i }}</td> --}}

											<td>{{ $unltweb->apellido }}</td>
											<td>{{ $unltweb->nombre }}</td>
                                            <td>{{ $unltweb->tp1_html }}</td>
											<td>{{ $unltweb->tp2_css }}</td>
											<td>{{ $unltweb->tp3_servidores }}</td>
											<td>{{ $unltweb->tp4_http }}</td>
											{{-- <td>{{ $unltweb->correo }}</td> --}}
											{{-- <td>{{ $unltweb->entorno }}</td>
											<td>{{ $unltweb->orientacion }}</td>
											<td>{{ $unltweb->conocimientos }}</td> --}}
										
											<td>{{ $unltweb->encuentro }}</td> 

                                            <td>
                                                <form action="{{ route('unltwebs.destroy',$unltweb->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('unltwebs.show',$unltweb->id) }}"><i class="fa fa-fw fa-eye"></i> Mostrar</a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('unltwebs.edit',$unltweb->id) }}"><i class="fa fa-fw fa-edit"></i> Editar</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> Borrar</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $unltwebs->links() !!}
            </div>
        </div>
    </div>
@endsection
