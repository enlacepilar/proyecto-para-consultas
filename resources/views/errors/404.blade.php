@extends('errors::minimal')

@section('title', __('No se encontró'))
@section('code', '404')
@section('message', __('No se encontró la página solicitada'))
