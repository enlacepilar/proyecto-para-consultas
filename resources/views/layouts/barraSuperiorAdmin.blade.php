<nav class="navbar navbar-expand-lg navbar-light alert-success mb-5" style="box-shadow: 0px 8px 10px grey;">
  <a class="navbar-brand" href="#">Sector del administrador</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="/unltwebs">Inicio<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/unltwebs/create">Nuevo Alumno</a>
      </li>
    </ul>

     <!-- Right Side Of Navbar -->
     <ul class="navbar-nav ml-auto">
      <!-- Authentication Links -->
      @guest
      @if (Route::has('login'))
          <li class="nav-item">
              <a class="nav-link" href="{{ route('login') }}"><span class="text-primary"><i class="fas fa-user-lock"></i> Admin</span></a>
          </li>
      @endif

      @else
      <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle text-primary" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Usuario: {{ Auth::user()->name }}
          </a>
          
          <div class="dropdown-menu animate__animated animate__fadeInUp" aria-labelledby="navbarDropdownMenuLink">
              {{-- <a class="dropdown-item alert-warning" href="/buscaUsuario">Buscar Usuario</a> --}}
           
              <a class="dropdown-item alert-warning" href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
                 Salir
              </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                      @csrf
                  </form>
          </div>
      </li>
      @endguest
  </ul>
</div>
<a class="btn btn-link" href="/"><span style="float: right; font-size: 12px; margin-right: 10px;color: rgb(44, 141, 226);"> <i class="fas fa-hand-point-left"></i> Volver a la pag. principal</span></a>
  </div>

 
</nav>