<!-- #region BARRA DE NAVEGACION-->
<nav class="navbar navbar-expand-lg navbar-light bg-primary mt-0">
    <div class="navbar-brand bg-primary">
        <a href="/"><h2 style="color: white;"><i class="fas fa-desktop" style="font-size: 30px;"></i> Servicio de consultas Enlacepilar</h2></a>
    </div>
        
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Menú de FUNCIONES
                    </button>
                    <div class="dropdown-menu animate__animated animate__lightSpeedInRight" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="/r">Funcion de prueba</a> 
                
                    
                    </div>
                </div>    
            </li>    
        </ul>

    <!-- Right Side Of Navbar -->
        <ul class="navbar-nav ml-auto">
            <!-- Authentication Links -->
            @guest
            @if (Route::has('login'))
                <li class="nav-item">
                    <a class="nav-link" href="/loguear"><span class="text-light"><i class="fas fa-user-lock"></i> Admin</span></a>
                </li>
            @endif

            @else
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Usuario: {{ Auth::user()->name }}
                </a>
                
                <div class="dropdown-menu animate__animated animate__fadeInUp" aria-labelledby="navbarDropdownMenuLink">
                   
                    <a class="dropdown-item alert-warning" href="/unltwebs">Listado de Alumnos</a>
                    <a class="dropdown-item alert-warning" href="/unltwebs/create">Nuevo Alumno</a>
                    <a class="dropdown-item alert-warning" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                       Salir
                    </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                </div>
            </li>
            @endguest
        </ul>
    </div>
        <a class="btn btn-link" href="/preguntasFrecuentes"><span style="float: right; font-size: 40px; margin-right: 10px;color: aliceblue;"><i class="far fa-question-circle"></i></span></a>
</nav>
<!-- #endregion-->

