<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <!-- CSRF Token -->
     <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Favicon-->
    <link rel="icon" type="image/jpg" href="/imagenes/pc.png">

    <!-- link a bootstrap, sweetalert2  y Datatables-->
    <link rel="stylesheet" href="/bootstrap-local/css/bootstrap.min.css">
    <!-- <link rel="stylesheet" href="bootstrap/PULSE-bootstrap.min.css"> -->
    <!-- <link rel="stylesheet" href="sweetAlert2/sweetalert2.min.css"> -->
    <link rel="stylesheet" href="/animatecss/animate.min.css">
    
    <!-- Datatables -->
    <link rel="stylesheet" href="/Datatables/jquery.dataTables.min.css">

    <!-- locales -->
    <link rel="stylesheet" href="/css/index.css">

    <!-- FontAwesome -->
    <link rel="stylesheet" href="/fontawesome-free-5.15.2-web/css/all.min.css">

    {{-- select2 --}}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

    <title>Sección del Administrador</title>
</head>
<body>
    

    <div>
        @yield('contenido_app')
    </div>
    <!-- #region ZONA DE SCRIPTS -->

    <!-- FONT AWESOME PARA ICONOS -->
    <script src="/fontawesome-free-5.15.2-web/js/all.min.js"></script>

    <!-- scripts a Jquery, bootstrap, sweetAlert2 y DataTables  -->
    <script src="/js/jquery-3.5.1.min.js"></script>
    <script src="/bootstrap-local/js/bootstrap.min.js"></script>
    <!-- <script src="sweetAlert2/sweetalert2.all.min.js"></script> -->

    <!-- DATATABLES -->
    <script src="/Datatables/jquery.dataTables.min.js"></script>
    <script src="/Datatables/dataTables.buttons.min.js"></script>
    <script src="/Datatables/jszip.min.js"></script>
    <script src="/Datatables/pdfmake.min.js"></script>
    <script src="/Datatables/vfs_fonts.js"></script>
    <script src="/Datatables/buttons.html5.min.js"></script>
    <script src="/Datatables/buttons.print.min.js"></script>

    {{-- select2 --}}
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <!-- archivo Javascript Local -->
    <!-- <script src="locales/app-principal.js?version=10.098"></script> -->
    <!-- <script src="./Recolecta/recolecta.js"></script> -->
    <!-- <script src="locales/maneja-enlaces.js"></script> -->
    <!-- <script src="locales/novedades.js"></script> -->
<!-- #endregion -->

@include('layouts.script_datatables')

<script>
    $(document).ready(function() {
        $('#usoSelect2').select2();
    });

    $(document).ready(function() {
        $('#usoSelect2-B').select2();
    });
    </script>
</body>
</html>